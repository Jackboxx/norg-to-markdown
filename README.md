# Norg to Markdown
This is a simple CLI tool to convert [.norg](https://github.com/nvim-neorg/norg-specs/blob/main/1.0-specification.norg) files to [GFM](https://github.github.com/gfm/). 

## Note
This tool is in the very early stages of development so many things don't work yet.

### Features
- [x] headers
- [ ] footnotes
- [x] highlighting (bold, italic)
- [x] unordered lists
- [x] ordered lists
- [x] checklists
- [ ] tables
- [x] links
- [ ] quotes
- [x] code blocks
- [ ] attributes

## Installation

### Manual
```shell
make  
make install
```
